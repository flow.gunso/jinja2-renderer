FROM python:3.11-alpine

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH "${PYTHONPATH}:/opt/lib"

COPY requirements.txt .
RUN pip install -r requirements.txt; \
    mkdir /out /in

COPY renderer /opt/lib/renderer

ENTRYPOINT [ "python3", "-m", "renderer", "--input", "/in/", "--output", "/out/" ]
CMD []
