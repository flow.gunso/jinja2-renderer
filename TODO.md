* Add logging
* Documentation
* Unit tests
* Release Python package
* Support for templates not suffixed with `.j2` ?
* Fix the SAST on branches ?