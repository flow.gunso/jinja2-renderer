# Maintenace page

This example shows how to live-code with the renderer.

## _templates/_ directory
That directory is where you may place your templates. The templates must use the Jinja2 format, hence the project name.

Subfolders are not supported yet.

The files must be suffixed with `.j2`.

## _render.toml_ file
The _render.toml_ contains the information used in the templates rendering.

A table refer to a template. Only the key/value pairs under it will be used for the template.

## _compose.yaml_ file
It contains two services:
* An nginx web server
* The renderer configurer to watch for changes and render at once.

The renderer has a [publicly availabe Docker image](registry.gitlab.com/flow.gunso/jinja2-renderer:1).
