# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!--## [Unreleased] | 2023/10/29
### Added
### Changed
### Obsolete
### Removed
### Fixed
### Security
-->

<!-- 1.0.3 -->
## [1.0.3] | 2024/08/18
### Changed
* Moved the sources to namespace _Florian Anceau / Open source software_.
* Published on Docker Hub at _flrnnc/jinja2-renderer_.
* Modified the CI variables to use the one's from the Gitlab group
### Removed
* Docker images on Gitlab's registry.

[1.0.3]: https://gitlab.com/florian.anceau-oss/jinja2-renderer/-/releases/1.0.3
<!-- /1.0.3 -->

## [1.0.2] | 2023/10/29
### Added
- Create the input and output directories in the Docker image
### Changed
- Lock to Python 3.11 in the Docker image
### Fixed
- Fix the empty output when using the `--watch` option in Docker

[1.0.2]: https://gitlab.com/florian.anceau-oss/jinja2-renderer/-/releases/1.0.2

## [1.0.1] | 2023/10/29
### Fixed
- Fixed the CI/CD release script (!2)

[1.0.1]: https://gitlab.com/florian.anceau-oss/jinja2-renderer/-/releases/1.0.1

## [1.0.0] | 2023/10/29
### Added
- The tool! (!1)

[1.0.0]: https://gitlab.com/florian.anceau-oss/jinja2-renderer/-/releases/1.0.0

