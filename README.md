# Jinja2 Renderer

It's a command-line tool to render Jinja2 templates with data stored in a TOML file.

Currently only available, production-wise, through a Docker image available in this repository's registry. But you welcome to clone the repository and have a go a it yourself.

## Usage

A Docker image is available on Docker Hub at:

```flrnnc/jinja2-renderer```

### Supported tags
[1](https://hub.docker.com/r/flrnnc/jinja2-renderer),
[1.0](https://hub.docker.com/r/flrnnc/jinja2-renderer),
[1.0.2](https://hub.docker.com/r/flrnnc/jinja2-renderer),
[latest](https://hub.docker.com/r/flrnnc/jinja2-renderer)
(see [Dockerfile](https://gitlab.com/florian.anceau-oss/jinja2-renderer/-/blob/1.0.2/Dockerfile))

### Examples

Checkout the [Maintenance page example](examples/maintenance-page/) for a detailled example and documentation.

## Contributing

The is a [roadmap](TODO.md) and a [change log](CHANGELOG.md).