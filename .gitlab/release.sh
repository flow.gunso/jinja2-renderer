#!/usr/bin/env bash

usage() {
    echo
    echo "Docker image release script for Jinja2 Renderer."
    echo
    echo "   [options]"
    echo
    echo "Available options are:"
    echo "  -h, --help      Display this usage helper."
    echo "  --production    Release Docker images for production."
    echo "  --commit        Release a Docker image tagged with the short commit hash only."
    echo

    if [ "$1" ]; then echo "Error: $1"; echo; exit 1; fi
    echo; exit 0
}

release_production() {
    tags=()

    tag=""
    for version_component in $(echo $CI_COMMIT_TAG | tr "." "\n"); do
        tag+="$version_component"
        tags+=("$tag")
        tag+="."
    done
    #tags+=("$CI_COMMIT_SHORT_SHA")
    tags+=("latest")

    for tag in "${tags[@]}"; do
        docker tag $DOCKER_REGISTRY_IMAGE:artifact $DOCKER_REGISTRY_IMAGE:$tag
        docker push $DOCKER_REGISTRY_IMAGE:$tag
    done
}

release_commit() {
    docker tag $DOCKER_REGISTRY_IMAGE:artifact $DOCKER_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    docker push $DOCKER_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
}

if [[ $# -eq 0 ]]; then usage; fi

while [[ $# -gt 0 ]]; do
  case $1 in
    --production)
      TARGET="production"
      shift
      ;;
    --commit)
      TARGET="commit"
      shift
      ;;
    -h|--help)
      usage
      ;;
    *)
      usage "Unknown option $1"
      ;;
  esac
done

docker login --username $DOCKER_REGISTRY_USERNAME --password $DOCKER_REGISTRY_TOKEN $CI_REGISTRY
docker build --tag $DOCKER_REGISTRY_IMAGE:artifact .

case "$TARGET" in
    "production")
        release_production
        ;;
    "commit")
        release_commit
        ;;
esac