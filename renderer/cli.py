
from pathlib import Path
import tomllib
import os

from click.exceptions import UsageError, BadParameter
import click

from .renderer import Renderer
from .watcher import Watcher


def validate_configuration(context, parameter, value):
    filename = "render.toml"
    default_paths = [os.getcwd(), "/etc", "/", ]
    if value is None:
        for path in default_paths:
            configuration = Path(path).joinpath(filename)
            if configuration.exists() and configuration.is_file():
                return configuration
        raise UsageError("Could not find any render.toml configuration. The search paths are the current working directory, /etc and / in that order.")

    configuration = Path(value)
    if configuration.exists() and configuration.is_file():
        return configuration
    raise BadParameter(f"Could not find {configuration} file.")


@click.command
@click.option("-i", "--input", type=click.Path(path_type=Path), default=Path("in/"))
@click.option("-o", "--output", type=click.Path(path_type=Path), default=Path("out/"))
@click.option("-c",
              "--configuration",
              type=click.Path(path_type=Path),
              callback=validate_configuration)
@click.option("--watch", is_flag=True)
def main(input: Path, output: Path, configuration: Path, watch: bool):

    renderer = Renderer(configuration, input, output)
    if watch:
        print("Watching for changes...")
        watcher = Watcher(renderer, input, configuration, 5)
        watcher.watch()
    else:
        print("Rendering all at once...")
        renderer.render_all()
