from pathlib import Path
import time
import re

from watchdog.events import FileSystemEventHandler, FileSystemEvent, FileClosedEvent
from watchdog.observers import Observer

from .renderer import Renderer


class Watcher:

    def __init__(self, renderer, input: Path, configuration_path: Path, interval: int) -> None:
        templates_event_handler = TemplatesEventHandler(renderer)
        configuration_event_handler = ConfigurationEventHandler(renderer)
        self.observer = Observer()
        self.observer.schedule(templates_event_handler, input)
        self.observer.schedule(configuration_event_handler, configuration_path)
        self.interval = interval

    def watch(self):
        self.observer.start()
        try:
            while True:
                time.sleep(self.interval)
        except KeyboardInterrupt:
            self.observer.stop()
            self.observer.join()


class RendererEventHandler(FileSystemEventHandler):

    def __init__(self, renderer: Renderer) -> None:
        self.renderer = renderer
        super().__init__()


class TemplatesEventHandler(RendererEventHandler):

    def on_closed(self, event: FileSystemEvent):
        if isinstance(event, FileClosedEvent):
            filename = Path(event.src_path).name
            print(f"Template {filename} was modified")
            self.renderer.render_file(filename)

class ConfigurationEventHandler(RendererEventHandler):

    def on_closed(self, event: FileSystemEvent):
        updated, changes = self.renderer.load_configuration()

        files_to_render = []
        if "dictionary_item_added" in changes:
            added = changes["dictionary_item_added"]
            files_added = parse_added(added)
            files_to_render += files_added
        if "values_changed" in changes:
            changed = changes["values_changed"]
            files_changed = parse_changed(changed)
            files_to_render += files_changed

        print(f"Configuration was modified.")
        print(f"Will update {', '.join(files_to_render)} was modified")
        self.renderer.render_files(files_to_render)

def parse_added(changes: list):
    filenames = []
    pattern = r"root\['(?P<filename>[\w\.-]+)'\]"
    for change in changes:
        match = re.search(pattern, change)
        filename = match.group("filename")
        filenames.append(filename)
    return filenames


def parse_changed(changes: dict):
    filenames = []
    pattern = r"root\['(?P<filename>[\w\.-]+)'\]\[.*\]"
    for change in changes.keys():
        match = re.search(pattern, change)
        filename = match.group("filename")
        filenames.append(filename)
    return filenames