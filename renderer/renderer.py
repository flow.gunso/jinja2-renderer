from typing import Dict, Tuple, List
from pathlib import Path
import tomllib

from deepdiff import DeepDiff
from jinja2 import Environment, FileSystemLoader, TemplateNotFound

Configuration = Dict[str, Dict[str, str]]

class Renderer:

    def __init__(self, configuration_path: Path, input: Path, output: Path) -> None:
        self.input = input
        self.output = output
        self.environment = Environment(loader=FileSystemLoader(input))
        self.configuration_path = configuration_path
        self.configuration = None
        self.load_configuration()

    def load_configuration(self) -> Tuple[bool, List[str]]:
        with open(self.configuration_path, "rb") as fo:
            configuration = tomllib.load(fo)

        if self.configuration is None:
            self.configuration = configuration
            return False, None
        
        diff = DeepDiff(self.configuration, configuration)
        return True, diff

    def render_all(self) -> None:
        for filename in self.configuration.keys():
            self.render_file(filename)
    
    def render_files(self, filenames: List[str]) -> None:
        for filename in filenames:
            self.render_file(filename)

    def render_file(self, filename: str) -> None:
        print(f"Rendering {filename}...")
        try:
            template = self.environment.get_template(filename)
        except TemplateNotFound:
            print(f"Warning: could not find template {filename}")
            return 
        
        context = self.configuration[filename]
        destination = self.output.joinpath(filename.removesuffix(".j2"))
        with open(destination, "wt") as fo:
            fo.write(template.render(context))